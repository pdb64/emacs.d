;;; pb-gui.el --- defaults for emacs in GUI mode.
;;;
;;; Commentary:
;;;  _  _     __  _ _  _  _  _  _  _
;;; / //_'/_///_// / //_|/ //_ /_'/
;;;
;;; Copyright (C) 2017 Patrick Arthur Brown
;;;
;;; Code:

(server-start)

;; No scroll-bars
(scroll-bar-mode -1)

;; Highlight the current line
(global-hl-line-mode t)

(provide 'pb-gui)
;;; pb-gui ends here
